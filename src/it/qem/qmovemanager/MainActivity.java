/*
 * Copyright (C) 2014 Silverio Diquigiovanni <shineworld.software@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.qem.qmovemanager;

import java.util.Locale;

import it.qem.qmovemanager.R;
import it.shineworld.qmove.qnet.QMoveAccess;
import it.shineworld.qmove.qnet.QMoveAccess.State;
import it.shineworld.sysutils.LogUtils;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	private MainHandler mHandler;
	private TextView mAccessoryState;
	private PlaceholderFragment mFragment;

	public QMoveAccess mQMoveAccess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LogUtils.track();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			mFragment = new PlaceholderFragment();
			getFragmentManager().beginTransaction().add(R.id.container, mFragment).commit();
		}

		mAccessoryState = (TextView) findViewById(R.id.accessory_state);
		mAccessoryState.setVisibility(View.VISIBLE);

		mQMoveAccess = new QMoveAccess(this);
		mQMoveAccess.start();

		mHandler = new MainHandler(this);
	}

	@Override
	protected void onDestroy() {
		LogUtils.track();
		super.onDestroy();
		if (mQMoveAccess != null)
			mQMoveAccess.stop();
	}

	@Override
	public void onPause() {
		LogUtils.track();
		super.onPause();
		if (mQMoveAccess != null)
			mQMoveAccess.removeStateChangeListener(mHandler);
	}

	@Override
	public void onResume() {
		LogUtils.track();
		super.onResume();
		if (mQMoveAccess != null)
			mQMoveAccess.addStateChangeListener(mHandler);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	public static class PlaceholderFragment extends Fragment {
		private TextView mCpuMode;
		private TextView mCpuSystemTime;
		private TextView mCpuUsedCodeMemory;
		private TextView mCpuUsedDataMemory;
		private TextView mCpuUsedRetentiveMemory;
		private TextView mActiveTask;
		private ImageView mCpuInfoWaitState;
		private ImageView mCpuInfoTimeTaskLost;
		private ImageView mCpuInfoBreakpoint;
		private ImageView mCpuInfoWatchdogTask;
		private ImageView mCpuWarningBatteryLow;
		private ImageView mCpuWarningTimeTaskLost;
		private ImageView mCpuWarningBackupOff;
		private ImageView mCpuWarningWatchdogTask;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			
			mCpuMode = (TextView) rootView.findViewById(R.id.cpu_mode);
			mCpuSystemTime = (TextView) rootView.findViewById(R.id.cpu_system_time);
			mCpuUsedCodeMemory = (TextView) rootView.findViewById(R.id.cpu_used_code_memory);
			mCpuUsedDataMemory = (TextView) rootView.findViewById(R.id.cpu_used_data_memory);
			mCpuUsedRetentiveMemory = (TextView) rootView.findViewById(R.id.cpu_used_retentive_memory);
			mActiveTask = (TextView) rootView.findViewById(R.id.active_task);
			mCpuInfoWaitState = (ImageView) rootView.findViewById(R.id.cpu_info_wait_state);
			mCpuInfoTimeTaskLost = (ImageView) rootView.findViewById(R.id.cpu_info_time_task_lost);
			mCpuInfoBreakpoint = (ImageView) rootView.findViewById(R.id.cpu_info_breakpoint);
			mCpuInfoWatchdogTask = (ImageView) rootView.findViewById(R.id.cpu_info_watchdog_task);
			mCpuWarningBatteryLow = (ImageView) rootView.findViewById(R.id.cpu_warning_battery_low);
			mCpuWarningTimeTaskLost = (ImageView) rootView.findViewById(R.id.cpu_warning_time_task_lost);
			mCpuWarningBackupOff = (ImageView) rootView.findViewById(R.id.cpu_warning_backup_off);
			mCpuWarningWatchdogTask = (ImageView) rootView.findViewById(R.id.cpu_warning_watchdog_task);

			return rootView;
		}
	}

	private static class MainHandler extends Handler implements Runnable {
		private MainActivity mActivity;
		private QMoveAccess mQMoveAccess;
		private String[] mAccessoryStates;

		public MainHandler(MainActivity activity) {
			super();
			LogUtils.track();
			mActivity = activity;
			mQMoveAccess = mActivity.mQMoveAccess;
			mAccessoryStates = mActivity.getResources().getStringArray(R.array.accessory_states);
		}

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
				case QMoveAccess.MESSAGE_STATE_CHANGED:
					State state = (State) msg.obj;
					switch (state) {
						case DISABLED:
						case ACCESSORY_DISCONNECTED:
						case ACCESSORY_WAIT_PERMISSION:
						case ACCESSORY_CONNECTED:
						case TARGET_CONNECTING:
						case TARGET_CHECKING:
						case TARGET_GET_CHECKSUMS:
						case TARGET_GET_SYMBOLS:
						case TARGET_CONNECTED:
							mActivity.mAccessoryState.setTextColor(mActivity.getResources().getColor(R.color.black));
							mActivity.mAccessoryState.setText(mAccessoryStates[state.ordinal()]);
							break;
						case TARGET_INCOMPATIBLE_API_LEVEL:
						case TARGET_ERROR:
							mActivity.mAccessoryState.setTextColor(mActivity.getResources().getColor(R.color.red));
							mActivity.mAccessoryState.setText(mAccessoryStates[state.ordinal()]);
							break;
					}

					if (state == State.TARGET_CONNECTED)
						post(this);
					else
						removeCallbacks(this);
			}
		}

		int mPhase;
		boolean mIsValid;
		boolean mNeedUpdate;
		QMoveAccess.SystemInfo mSystemInfo;

		int mHandle = QMoveAccess.REQUEST_HANDLE_INVALID;

		@Override
		public void run() {
			if (mQMoveAccess == null || mQMoveAccess.getState() != QMoveAccess.State.TARGET_CONNECTED) {
				mNeedUpdate = true;
				mIsValid = false;
			}
			else {
				switch (mPhase) {
					case 0:
						mHandle = mQMoveAccess.refreshSystemInfo();
						mPhase++;
						break;
					case 1:
						if (mQMoveAccess.checkForHandle(mHandle, false)) {
							mQMoveAccess.releaseHandle(mHandle);
							mSystemInfo = mQMoveAccess.getSystemInfo();
							mHandle = mQMoveAccess.refreshSystemInfo();
							mNeedUpdate = true;
							mIsValid = true;
						}
						break;
				}
			}
			refreshObjects();
			postDelayed(this, 1);
		}

		void refreshObjects() {
			PlaceholderFragment fragment = mActivity.mFragment;
			if (mNeedUpdate) {
				mNeedUpdate = false;
				if (mIsValid) {
					if (mSystemInfo != null) {
						fragment.mCpuMode.setText(String.valueOf(mSystemInfo.cpuMode.toString()));
						fragment.mCpuSystemTime.setText(msTimeToString(mSystemInfo.systemTime));
						fragment.mCpuUsedCodeMemory.setText(String.valueOf(mSystemInfo.usedCodeMemory) + "%");
						fragment.mCpuUsedDataMemory.setText(String.valueOf(mSystemInfo.usedDataMemory) + "%");
						fragment.mCpuUsedRetentiveMemory.setText(String.valueOf(mSystemInfo.usedRetentiveDataMemory) + "%");
						String taskName = mQMoveAccess.getUnitName(mSystemInfo.activeTaskNumber);
						if (taskName == null)
							taskName = "[ " + String.valueOf(mSystemInfo.activeTaskNumber) + " ]";
						fragment.mActiveTask.setText(taskName);
						fragment.mCpuInfoWaitState.setImageLevel((mSystemInfo.activeTaskFlags & 0x02) != 0 ? 1 : 0);
						fragment.mCpuInfoTimeTaskLost.setImageLevel((mSystemInfo.activeTaskFlags & 0x04) != 0 ? 1 : 0);
						fragment.mCpuInfoBreakpoint.setImageLevel((mSystemInfo.activeTaskFlags & 0x01) != 0 ? 1 : 0);
						fragment.mCpuInfoWatchdogTask.setImageLevel((mSystemInfo.activeTaskFlags & 0x10) != 0 ? 1 : 0);
						fragment.mCpuWarningBatteryLow.setImageLevel((mSystemInfo.cpuWarning & 0x02) != 0 ? 1 : 0);
						fragment.mCpuWarningTimeTaskLost.setImageLevel((mSystemInfo.cpuWarning & 0x08) != 0 ? 1 : 0);
						fragment.mCpuWarningBackupOff.setImageLevel((mSystemInfo.cpuWarning & 0x04) != 0 ? 1 : 0);
						fragment.mCpuWarningWatchdogTask.setImageLevel((mSystemInfo.cpuWarning & 0x01) != 0 ? 1 : 0);
						
					} else {
						String s = mActivity.getString(R.string.unavailable_value);
						fragment.mCpuMode.setText(s);
						fragment.mCpuSystemTime.setText(s);
						fragment.mCpuSystemTime.setText(s);
						fragment.mCpuUsedCodeMemory.setText(s);
						fragment.mCpuUsedDataMemory.setText(s);
						fragment.mCpuUsedRetentiveMemory.setText(s);
						fragment.mActiveTask.setText(s);
						fragment.mCpuInfoWaitState.setImageLevel(0);
						fragment.mCpuInfoTimeTaskLost.setImageLevel(0);
						fragment.mCpuInfoBreakpoint.setImageLevel(0);
						fragment.mCpuInfoWatchdogTask.setImageLevel(0);
						fragment.mCpuWarningBatteryLow.setImageLevel(0);
						fragment.mCpuWarningTimeTaskLost.setImageLevel(0);
						fragment.mCpuWarningBackupOff.setImageLevel(0);
						fragment.mCpuWarningWatchdogTask.setImageLevel(0);
					}
				} else {
					String s = mActivity.getString(R.string.unavailable_value);
					fragment.mCpuMode.setText(s);
					fragment.mCpuSystemTime.setText(s);
					fragment.mCpuSystemTime.setText(s);
					fragment.mCpuUsedCodeMemory.setText(s);
					fragment.mCpuUsedDataMemory.setText(s);
					fragment.mCpuUsedRetentiveMemory.setText(s);
					fragment.mActiveTask.setText(s);
					fragment.mCpuInfoWaitState.setImageLevel(0);
					fragment.mCpuInfoTimeTaskLost.setImageLevel(0);
					fragment.mCpuInfoBreakpoint.setImageLevel(0);
					fragment.mCpuInfoWatchdogTask.setImageLevel(0);
					fragment.mCpuWarningBatteryLow.setImageLevel(0);
					fragment.mCpuWarningTimeTaskLost.setImageLevel(0);
					fragment.mCpuWarningBackupOff.setImageLevel(0);
					fragment.mCpuWarningWatchdogTask.setImageLevel(0);
				}
			}
		}
		
		String msTimeToString(long value) {
			if (value == 0)
				return "0h 00' 00\" 000'";
			long h = value / 3600000;
			if (h != 0)
				value -= h * 3600000;
			long m = value / 60000;
			if (m != 0)
				value -= m * 60000;
			long s = value / 1000;
			if (s != 0)
				value -= s * 1000;
			return String.format(Locale.US, "%dh %02d' %02d\" %03d", h, m, s, value);
		}
	};

}
